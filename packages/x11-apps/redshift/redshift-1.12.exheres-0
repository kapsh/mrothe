# Copyright 2014 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 1.13 ] ]
require github [ user="jonls" tag="v${PV}" ]
require python [ blacklist="2" multibuild=false with_opt=true option_name="gui" ]
require gtk-icon-cache systemd-service

SUMMARY="Adjusts the color temperature of your screen according to your surroundings"
HOMEPAGE="http://jonls.dk/redshift/"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    geoclue [[ description = [ Use geoclue to get the current location ] ]]
    gui [[ description = [ Enable GUI status icon ] ]]
    ( linguas: ar bg ca cs da de el es et eu fi fr gl he hi hr it ja ka lt nb nl pl pt_BR pt ru sv zh_CN )
"

DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext[>=0.17]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.26]
        geoclue? ( gps/geoclue:2.0 )
        gui? (
            dev-python/pyxdg[python_abis:*(-)?]
            gnome-bindings/pygobject:3[python_abis:*(-)?]
            x11-libs/gtk+:3[gobject-introspection]
        )
        x11-dri/libdrm
        x11-libs/libX11
        x11-libs/libXrandr
        x11-libs/libXxf86vm
        x11-libs/libxcb
"

AT_M4DIR=( m4 )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-drm'
    '--enable-nls'
    '--enable-randr'
    '--enable-vidmode'
    "--with-systemduserunitdir=${SYSTEMDUSERUNITDIR}"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'geoclue geoclue2'
    'gui'
)

src_prepare() {
    edo autopoint --force
    edo intltoolize --force --copy --automake
    autotools_src_prepare
}

